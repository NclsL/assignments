import unittest
import os.path
from fibonacci import Fibonacci

class FibonacciTest(unittest.TestCase):

    def setUp(self):
        self.file_name = "fibonacci_series.txt"
        self.fib_1 = Fibonacci(0, 10) #Series from indexes 0 to 10
        self.fib_2 = Fibonacci(4, 24) #Series from indexes 0 to 10, skipping numbers lower than 4
        self.fib_3 = Fibonacci(2, "this shouldn't be here") #Faulty input
        self.fib_4 = Fibonacci(0, 30) #Large number example
        self.fib_5 = Fibonacci(0, -3) #Negative input


    def tearDown(self):
        pass


    #Check that series are calculated without errors
    def test_case_calculate_series(self):
        print("TEST CASE: Calculate series\n")
        print(self.fib_1.calculate_series())
        print(self.fib_2.calculate_series())


    def test_nth_number_is_correct(self):
        print("TEST CASE: Check nth number\n")

        fibonacci = self.fib_1.calculate_series()
        self.assertEqual(fibonacci[9], 34)

        large_fibonacci = self.fib_4.calculate_series()
        self.assertEqual(large_fibonacci[27], 196418) #verified with https://www.dcode.fr/fibonacci-numbers


    def test_case_check_output_file_exists(self):
        print("TEST CASE: Check output file\n")
        self.fib_1.calculate_series()
        self.fib_1.save_to_file(self.file_name)
        self.assertTrue(os.path.exists(self.file_name))


    def test_case_faulty_input(self):
        print("TEST CASE: Faulty input\n")
        self.assertRaises(ValueError, self.fib_3.calculate_series)
        self.assertRaises(ValueError, self.fib_5.calculate_series)


    def test_file_nth_number_is_correct(self):
        self.fib_4.calculate_series()
        self.fib_4.save_to_file("large_series.txt")
        try:
            with open("large_series.txt", "r") as file:
                lines = file.readlines()
                self.assertTrue(lines[25], 75025)
        except IOError:
            raise SystemExit("Couldn't open file")


if __name__ == '__main__':
    unittest.main()