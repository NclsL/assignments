class Fibonacci:

    def __init__(self, starting_number, n=10):
        self.starting_number = starting_number
        self.memo = []
        self.n = n


    def save_to_file(self, file_name):
        if not self.memo:
            raise SystemExit("No numbers to write to file. Exiting...\n")
        try:
            with open(file_name, 'w') as file:
                file.write('\n'.join(str(num) for num in self.memo))
        except IOError:
            raise SystemExit("Couldn't write file\n")
        finally:
            print("File written as " + str(file_name) + "\n")
            file.close()


    def calculate_series(self):
        if not isinstance(self.starting_number, int) or not isinstance(self.n, int):
            raise ValueError("One or more of the inputs was of wrong type\n")

        if self.n < 0:
            raise ValueError("Negative input not allowed\n")

        if self.n == 0:
            return 0
        if self.n == 1:
            return 1

        self.memo = [0 for _ in range(self.n)]
        self.memo[0] = 0
        self.memo[1] = 1
        for i in range(2, self.n):
            self.memo[i] = self.memo[i - 1] + self.memo[i - 2]
        self.memo = [x for x in self.memo if x >= self.starting_number]

        if not self.memo:
            raise SystemExit("Starting number was larger than any element on the list\n")
        return self.memo